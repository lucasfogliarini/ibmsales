using IBMSales.Core;
using System;
using System.IO;
using System.Linq;
using Xunit;

namespace IBMSales.Tests
{
    public class SalesReportServiceTest
    {
        readonly SalesReportService _salesReportService;

        public SalesReportServiceTest()
        {
            _salesReportService = new SalesReportService();
        }


        [Fact]
        public void GetBestSale_Should_Get()
        {
            string[] lines = GenerateContent();

            _salesReportService.ProcessOutContent(lines);
            var bestSale = _salesReportService.GetBestSale();

            Assert.Equal(185.6m, bestSale.TotalPrice);
        }

        [Fact]
        public void GetWorstSeller_Should_Get()
        {
            string[] lines = GenerateContent();

            _salesReportService.ProcessOutContent(lines);
            var worstSeller = _salesReportService.GetWorstSeller();

            Assert.Equal("Paulo", worstSeller.Name);
        }

        [Fact]
        public void ProcessOutContent_Should_Process()
        {
            string[] lines = GenerateContent();

            _salesReportService.ProcessOutContent(lines);

            Assert.Equal(2,_salesReportService.Sellers.Count);
            Assert.Equal(2, _salesReportService.Customers.Count);
            Assert.Equal(2, _salesReportService.Sales.Count);
        }

        [Fact]
        public void Run_Should_Run()
        {
            var inFilePath = Path.Combine(_salesReportService.InPath, "in.txt");
            var contentLines = GenerateContent();

            File.WriteAllLines(inFilePath, contentLines);
            _salesReportService.Run();
            System.Threading.Thread.Sleep(3000);

            var inFiles = Directory.GetFiles(_salesReportService.InPath);
            var outFiles = Directory.GetFiles(_salesReportService.OutPath);
            Assert.True(inFiles.Length == 0);
            Assert.True(outFiles.Length > 0);
        }

        [Fact]
        public void Process_Should_Process_In_Directory()
        {
            var inFilePath = Path.Combine(_salesReportService.InPath, "in.txt");
            var contentLines = GenerateContent();

            File.WriteAllLines(inFilePath, contentLines);
            _salesReportService.Process();

            var inFiles = Directory.GetFiles(_salesReportService.InPath);
            var outFiles = Directory.GetFiles(_salesReportService.OutPath);
            Assert.True(inFiles.Length == 0);
            Assert.True(outFiles.Length > 0);
        }

        [Fact]
        public void IdentifyEntity_Should_Identify()
        {
            var customerInput = "001�1234567891234�Pedro�50000";

            string entityCode = _salesReportService.IdentifyEntity(customerInput);

            Assert.NotNull(entityCode);
        }

        [Fact]
        public void IdentifyEntity_Should_ThrowException()
        {
            var customerInput = "";

            Assert.Throws<ApplicationException>(() =>
            {
                string entityCode = _salesReportService.IdentifyEntity(customerInput);
            });
        }

        [Fact]
        public void BuildSale_Should_Build()
        {
            var saleInput = "003�10�[1-10-100,2-30-2.50,3-40-3.10]�Pedro";

            Sale sale = _salesReportService.BuildSale(saleInput);

            Assert.NotNull(sale);
        }

        [Fact]
        public void BuildSaleItems_Should_Build()
        {
            var saleItemsInput = "[1-10-100,2-30-2.50,3-40-3.10]";

            var saleItems = _salesReportService.BuildSaleItems(saleItemsInput, 1).ToList();

            Assert.NotNull(saleItems);
        }

        [Fact]
        public void BuildCustomer_Should_Build()
        {
            var customerInput = "002�2345675434544345�Jose da Silva�Rural";

            Customer customer = _salesReportService.BuildCustomer(customerInput);

            Assert.NotNull(customer);
        }

        [Fact]
        public void BuildSeller_Should_Build()
        {
            var sellerInput = "001�1234567891234�Pedro�50000";

            Seller seller = _salesReportService.BuildSeller(sellerInput);

            Assert.NotNull(seller);
        }

        private string[] GenerateContent()
        {
            return new[]
            {
                "001�1234567891234�Pedro�50000",
                "001�3245678865434�Paulo�40000.99",
                "002�2345675434544345�Jose da Silva�Rural",
                "002�2345675433444345�Eduardo Pereira�Rural",
                "003�10�[1-10-100,2-30-2.50,3-40-3.10]�Pedro",
                "003�08�[1-34-10,2-33-1.50,3-40-0.10]�Paulo",
            };
        }
    }
}
