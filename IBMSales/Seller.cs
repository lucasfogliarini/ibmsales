﻿namespace IBMSales.Core
{
    public class Seller
    {
        public const string CODE = "001";
        public const string LINE_PATTERN = "001çCPFçNameçSalary";
        public string CPF { get; set; }
        public string Name { get; set; }
        public decimal Salary { get; set; }
    }
}
