﻿namespace IBMSales.Core
{
    public class SaleReportEventArgs
    {
        public string FilePath { get; internal set; }
    }
}
