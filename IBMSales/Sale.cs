﻿using System.Collections.Generic;
using System.Linq;

namespace IBMSales.Core
{
    public class Sale
    {
        public const string CODE = "003";
        public const string LINE_PATTERN = "003çIdç[Id-Quantity-Price]çSalesman";
        public int Id { get; set; }
        public string Salesman { get; set; }
        public IEnumerable<SaleItem> Items { get; set; }

        public decimal TotalPrice
        {
            get
            {
                return Items.Sum(i => i.Quantity + i.Price);
            }
        }
    }
}
