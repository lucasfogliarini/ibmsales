﻿namespace IBMSales.Core
{
    public class SaleItem
    {
        public const string LINE_PATTERN = "[Id-Quantity-Price,Id2-Quantity2-Price2]";
        public int Id { get; set; }
        public int SaleId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
    }
}
