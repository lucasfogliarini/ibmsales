﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Timers;

namespace IBMSales.Core
{
    public class SalesReportService
    {
        public string OutPath { get; private set; }
        public string InPath { get; private set; }
        public IList<Seller> Sellers { get; private set; } = new List<Seller>();
        public IList<Customer> Customers { get; private set; } = new List<Customer>();
        public IList<Sale> Sales { get; private set; } = new List<Sale>();

        public event EventHandler<SaleReportEventArgs> ReportProcessed;

        readonly Timer _timer = new Timer();
        public SalesReportService(string appPath = null)
        {
            CreateDirectories(appPath);
            _timer.Elapsed += Timer_Elapsed;
            _timer.Start();
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Process();
        }

        public Sale GetBestSale()
        {
            var salesOrdered = Sales.OrderByDescending(s => s.TotalPrice);
            return salesOrdered.FirstOrDefault();
        }

        public Seller GetWorstSeller()
        {
            var salesOrdered = Sales.OrderByDescending(s => s.TotalPrice);
            var worstSale = salesOrdered.LastOrDefault();

            return Sellers.FirstOrDefault(e => e.Name == worstSale.Salesman);
        }

        public string ProcessOutContent(string[] entityLines)
        {
            foreach (var line in entityLines)
            {
                var entityCode = IdentifyEntity(line);
                switch (entityCode)
                {
                    case Seller.CODE:
                        var seller = BuildSeller(line);
                        Sellers.Add(seller);
                        break;
                    case Customer.CODE:
                        var customer = BuildCustomer(line);
                        Customers.Add(customer);
                        break;
                    case Sale.CODE:
                        var sale = BuildSale(line);
                        Sales.Add(sale);
                        break;
                }
            }

            return GenerateOutContent();
        }

        private void CreateDirectories(string appPath)
        {
            if (appPath == null)
            {
                var desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                appPath = Path.Combine(desktopPath, DEFAULT_APP_PATH);
            }

            OutPath = Path.Combine(appPath, "data", "out");
            InPath = Path.Combine(appPath, "data", "in");

            Directory.CreateDirectory(OutPath);
            Directory.CreateDirectory(InPath);
        }

        public void Run(double interval = 2000)
        {
            _timer.Interval = interval;
            _timer.Enabled = true;
        }

        public void Stop()
        {
            _timer.Enabled = false;
        }

        public void Process()
        {
            var files = Directory.GetFiles(InPath);
            foreach (var inFile in files)
            {
                var entityLines = File.ReadAllLines(inFile);
                string processedContent = ProcessOutContent(entityLines);
                var timeProcessed = DateTime.Now.ToString("MMddyyyy_HHmmss");
                var outFile = Path.Combine(OutPath, $"processed_{timeProcessed}");
                File.WriteAllText(outFile, processedContent);
                File.Delete(inFile);                
                ReportProcessed?.Invoke(this, new SaleReportEventArgs()
                {
                    FilePath = outFile
                });
                ClearEntities();
            }
        }

        public string GenerateOutContent()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Customers Quantity: {Customers.Count}");
            sb.AppendLine($"Sellers Quantity: {Sellers.Count}");
            sb.AppendLine($"Best Sale Id: {GetBestSale().Id}");
            sb.AppendLine($"Worst Seller: {GetWorstSeller().Name}");
            return sb.ToString();
        }

        public string IdentifyEntity(string entityLine)
        {
            var entityLineSplitted = entityLine.Split(COLUMN_SEPARATOR);
            var entityCode = entityLineSplitted.First();
            var entityCodes = new[] { Customer.CODE, Seller.CODE, Sale.CODE };            
            bool entityExists = entityCodes.Contains(entityCode);
            if (!entityExists)
            {
                throw new ApplicationException($"Entity don't exists, must contain one of the following codes: {string.Join(", ", entityCodes)}");
            }
            return entityCode;
        }

        public Sale BuildSale(string saleInput)
        {
            var saleInputSplitted = saleInput.Split(COLUMN_SEPARATOR);
            if (saleInputSplitted.Length != 4)
            {
                throw new ApplicationException($"The Sale must have the pattern {Sale.LINE_PATTERN}.");
            }

            int saleId = Convert.ToInt32(saleInputSplitted[1]);
            return new Sale()
            {
                Id = saleId,
                Salesman = saleInputSplitted[3],
                Items = BuildSaleItems(saleInputSplitted[2], saleId).ToList(),
            };
        }

        public IEnumerable<SaleItem> BuildSaleItems(string saleItemsInput, int saleId)
        {
            saleItemsInput = saleItemsInput.Replace("[", "").Replace("]", "");
            var saleItemsInputSplitted = saleItemsInput.Split(ITEMS_SEPARATOR);
            if (saleItemsInputSplitted.Length != 3)
            {
                throw new ApplicationException($"The Sale Items must have the pattern {SaleItem.LINE_PATTERN}.");
            }
            foreach (var saleItem in saleItemsInputSplitted)
            {
                var saleItemInputSplitted = saleItem.Split(ITEM_SEPARATOR);
                yield return new SaleItem()
                {
                    Id = Convert.ToInt32(saleItemInputSplitted[0]),
                    Quantity = Convert.ToInt32(saleItemInputSplitted[1]),
                    Price = Convert.ToDecimal(saleItemInputSplitted[2]),
                    SaleId = saleId
                };
            }
        }

        public Customer BuildCustomer(string customerInput)
        {
            var customerInputSplitted = customerInput.Split(COLUMN_SEPARATOR);
            if (customerInputSplitted.Length != 4)
            {
                throw new ApplicationException($"The Customer must have the pattern {Customer.LINE_PATTERN}.");
            }

            return new Customer()
            {
                CPF = customerInputSplitted[1],
                Name = customerInputSplitted[2],
                BusinessArea = customerInputSplitted[3],
            };
        }

        public Seller BuildSeller(string sellerInput)
        {
            var sellerInputSplitted = sellerInput.Split(COLUMN_SEPARATOR);
            if (sellerInputSplitted.Length != 4)
            {
                throw new ApplicationException($"The Seller must have the pattern {Seller.LINE_PATTERN}.");
            }

            return new Seller()
            {
                CPF = sellerInputSplitted[1],
                Name = sellerInputSplitted[2],
                Salary = Convert.ToDecimal(sellerInputSplitted[3]),
            };
        }

        private void ClearEntities()
        {
            Sellers.Clear();
            Customers.Clear();
            Sales.Clear();
        }

        public const string DEFAULT_APP_PATH = "IBMSales";
        public const string COLUMN_SEPARATOR = "ç";
        public const string ITEMS_SEPARATOR = ",";
        public const string ITEM_SEPARATOR = "-";
    }
}
