﻿namespace IBMSales.Core
{
    public class Customer
    {
        public const string CODE = "002";
        public const string LINE_PATTERN = "002çCPFçNameçBusinessArea";
        public string CPF { get; set; }
        public string Name { get; set; }
        public string BusinessArea { get; set; }
    }
}
