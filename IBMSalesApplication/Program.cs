﻿using IBMSales.Core;
using System;

namespace IBMSales.Application
{
    class Program
    {
        static void Main(string[] args)
        {
            var salesReportService = new SalesReportService();
            salesReportService.ReportProcessed += SalesReportService_ReportProcessed; 
            salesReportService.Run();
            Console.WriteLine("IBM Sales is running!");
            Console.ReadLine();
        }

        private static void SalesReportService_ReportProcessed(object sender, SaleReportEventArgs e)
        {
            Console.WriteLine($"A Report file was processed: {e.FilePath}");
        }
    }
}
